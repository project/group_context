<?php

/**
 * @file
 * Contains the group context argument default plugin.
 */

/**
 * The group context argument default handler.
 */
class group_context_plugin_argument_default_group_context extends views_plugin_argument_default {

  /**
   * Retrieve the options when this is a new access
   * control plugin
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['group_type'] = array('default' => 'group');

    return $options;
  }

  /**
   * Provide the default form for setting options.
   */
  function options_form(&$form, &$form_state) {
    
  }

  /**
   * Return the group context argument.
   */
  function get_argument() {

    if ($group = group_context($this->options['group_type'])) {

      return $group['gid'];
    }
    return FALSE;
  }

}
