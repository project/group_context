<?php


/**
 * @file
 * Provides support for the Views module.
 */

/**
 * Implements hook_views_plugins().
 */
function group_context_views_plugins() {
  return array(
    'argument default' => array(
      'group_context' => array(
        'title' => t('Current Group from context'),
        'handler' => 'group_context_plugin_argument_default_group_context',
        'parent' => 'views_plugin_argument_default',
      ),
    ),
  );
  
}